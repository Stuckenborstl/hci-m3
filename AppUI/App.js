import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import ContentOverviewScreen from "./app/screens/contentOverviewScreen";
import PersonalisationScreen from "./app/screens/personalisationScreen";
import TopicScreen from "./app/screens/topicScreen";
import LoginScreen from "./app/screens/loginScreen";
import ProfilScreen from "./app/screens/profilScreen";
import { LogBox } from "react-native";
import { User } from "./app/objects/user";
import { AuthContext } from "./app/authContext";

const Tab = createBottomTabNavigator();

LogBox.ignoreAllLogs(); //used to hide warnings in UI

global.user = new User(); //User Object, used globally

export default function App() {

    const [loggedIn, setLoggedIn] = React.useState(false); //set by loginScreen, used to change Tab-Screens

    //screenOptions: used Icons in Navigation
    //I found no way to hide navigation-bar in loginScreen
    return (
        <AuthContext.Provider value={[loggedIn, setLoggedIn]}>
            <NavigationContainer>
                <Tab.Navigator
                    screenOptions={({ route }) => ({
                        tabBarIcon: () => {
                            let iconName;
                            if (route.name === "Themen") iconName = "search";
                            else if (route.name == "Personalisierung") iconName = "lightbulb-o";
                            else if (route.name == "Profil") iconName = "user";
                            else iconName = "book";
                            return <FontAwesome name={iconName} size={28} />;
                        },
                    })}
                >
                    {!loggedIn ? (
                        <Tab.Screen name="Login" component={LoginScreen} />
                    ) : (
                        <>
                            <Tab.Screen name="Themen" component={TopicScreen} />
                            <Tab.Screen name="Personalisierung" component={PersonalisationScreen} />
                            <Tab.Screen name="Inhalte" component={ContentOverviewScreen} />
                            <Tab.Screen name="Profil" component={ProfilScreen} />
                        </>
                    )}
                </Tab.Navigator>
            </NavigationContainer>
        </AuthContext.Provider>
    );
}
