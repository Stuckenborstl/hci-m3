## Readme - M3
* Gruppe:	    4
* Team-Nr.:     /
* Projektthema: LearnEasy

### Implementierung

Framework:	    React Native (Android und iOS (1 Feature geht nur auf Android))

API-Version:	va. auf Android API-Version 32 getestet.

GerÃ¤t(e), auf dem(denen) getestet wurde:
Pixel 12 Simulator, IPhone 11, IPhone8, Xiaomi poco x3

Externe Libraries und Frameworks:
- react-native-vector-icons/FontAwesome
- react-native-vector-icons/FontAwesome5
- react-native-social-buttons
- react-native-elements
- react-native-slider - Warnung weil veraltet, sollte kein Problem sein
- native-base
- @blump-tech/native-base-select - eventuell macht dieses Package Probleme, wir sind Leider zu spät drauf gekommen (bei Pixel 12 Simulator - API-Version 32 funktioniert alles bestens)

Dauer der Entwicklung:
- Paul ca. 30 - 35
- André ca. 6
- Kilian ca. 4
- Reka ca. 12

Weitere Anmerkungen:
- Diese App nutzt die 2 Objekte user.ts und content.ts
    - user.ts wird anfangs in App.js erstellt und danach von allen Screens genutzt
      jeder Screen setzt dabei bestimmte Variablen (topicScreen: Ids für benutzte Screens - personalisationScreen: time und priority je topic - Login: UserName (evtl. Password) - die Anderen beiden lesen aus dem User Objekt)
    - content.ts sammelt für alles an gesammeltem Content, unterteilt in Topics und weiter in Content-Arten (Bücher, Filme, ...)
- Login setzt nur UserName und Ändert NavigationScreens in App.js
- Quiz Layout fertig, die Sammlung von Fragen haben wir nichtmehr geschaft
- Die Idee der Personalisierung ist es, die im ContentOverviewScreen dargestellten Content-Entitys zu ortnen, evtl. welche zu verstecken (e.g. wenn schon gelesen), ...
  Da man dafür noch viel mehr Content-Entitys benötigen würde haben wir es noch nicht implementiert