import { StyleSheet } from "react-native";
import { colors } from "./colors";

//Globally usable StyleSheet

module.exports = StyleSheet.create({
    scrollView: {
        padding: 10,
        paddingTop: 30,
        flex: 1,
        flexDirection: "column",
        backgroundColor: "white", //'#EBFEFF'
        //justifyContent: "center",
    },
    centeredView: {
        padding: 10,
        paddingTop: 30,
        flex: 1,
        flexDirection: "column",
        backgroundColor: "white", //'#EBFEFF',
        alignItems: "center",
    },
    scrollViewFullW: {
        // scroll view full width
        paddingVertical: 10,
        paddingTop: 30,
        flex: 1,
        flexDirection: "column",
        backgroundColor: "white", //'#EBFEFF'
    },
    input: {
        textAlign: "left",
        paddingLeft: 20,
        height: 30,
        margin: 10,
        borderRadius: 20,
        width: "100%",
        backgroundColor: "#B4DFE5", //"#F5834E",
        color: "black",
        fontWeight: "bold",
    },
    inputDisabled: {
        textAlign: "left",
        paddingLeft: 20,
        height: 30,
        margin: 10,
        borderRadius: 20,
        width: "100%",
        backgroundColor: "#D3D3D3", //"#F5834E",
        color: "black",
        fontWeight: "bold",
    },
    buttonView: {
        padding: 15,
    },
    submBut: {
        color: colors.blueDark,
        backgroundColor: colors.blueDark,
    },
    simpleText: {
        fontSize: 18,
        padding: 10,
    },
    screenTitle: {
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        paddingBottom: 10,
        color: colors.titleBlue,
    },
    screenUntTitle: {
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center",
        paddingBottom: 5,
        color: colors.titleBlue,
    },
    popUp: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    profileScreen: {
        fontWeight: "bold",
        textAlign: "center",
    },
    profileScreenName: {
        padding: 7,
        fontSize: 18,
        textAlign: "center",
        fontWeight: "bold",
    },
    profileScreenInfos: {
        fontSize: 16,
        fontWeight: "bold",
        paddingTop: 10,
        backgroundColor: "#B4DFE5",
        //backgroundColor: "#6Fa4AB",
        flexDirection: "column",
        //flex: 6,
        height: 50,
        textAlign: "center",
        //color: "#6Fa4AB",
        //marginBottom:1,
    },
    profileScreenInfos2: {
        fontSize: 16,
        fontWeight: "bold",
        paddingTop: 10,
        backgroundColor: "#B4DFE5",
        //backgroundColor: "#6Fa4AB",
        //flexDirection: "column",
        //flex: 6,
        height: 50,
        textAlign: "center",
        justifyContent: "space-around",
        flexDirection: "row",
        //color: "#6Fa4AB",
        //marginBottom: 1,
        //marginTop :1,
    },
    profileScreenInfos3: {
        fontSize: 16,
        fontWeight: "bold",
        paddingTop: 15,
        backgroundColor: "#B4DFE5",
        //backgroundColor: "#6Fa4AB",
        flexDirection: "column",
        flex: 6,
        //height: 50,
        textAlign: "center",
        justifyContent: "space-around",
        flexDirection: "row",
        //color: "#6Fa4AB",
        //marginTop: 0,
        //marginBottom: 0,
    },
});

/*
fehler: '#F4976C',  // orange
warning: '#FBE8A6', // yellow
blueDark: '#303C6C',
blueLight1: '#B4DFE5',
blueLight2: '#D2FDFF',
*/

//F4976C
//FBE8A6
//B4DFE5
//303C6C
