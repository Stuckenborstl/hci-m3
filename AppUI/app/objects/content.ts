//Globally used Class
//Contains Arrays of all the collected Content, seperated by Topics and by Content-Types (so far Books, Films and Articles)

export class Content {
    constructor() {
        this.topics = [
            {
                name: "Griechische Mythologie",
                Buecher: [
                    {
                        name: "Mythologie der Griechen",
                        seiten: 768,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1028384851?ProvID=11010474",
                        bild: "https://assets.thalia.media/img/artikel/8dc7133b0be6b945c95b3b0da0c72ec819d37b1a-00-00.jpeg",
                        priority: 6,
                        zeiteinheiten: 30,
                    },
                    {
                        name: "Homer, Odyssee",
                        seiten: 336,
                        url: "https://www.amazon.de/Odyssee-Cabra-Ledereinband-Homer/dp/3730602918/ref=asc_df_3730602918/?tag=googshopde-21&linkCode=df0&hvadid=310608527232&hvpos=&hvnetw=g&hvrand=14183919421651906988&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9062727&hvtargid=pla-525095435255&psc=1&th=1&psc=1&tag=&ref=&adgrpid=61387764946&hvpone=&hvptwo=&hvadid=310608527232&hvpos=&hvnetw=g&hvrand=14183919421651906988&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9062727&hvtargid=pla-525095435255",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/41-myBhQk3S.jpg",
                        priority: 3,
                        zeiteinheiten: 22,
                    },
                    {
                        name: "Die schönsten Sagen des klassischen Altertums",
                        seiten: 636,
                        url: "https://www.morawa.at/detail/ISBN-9783785582756/Schwab-Gustav/Die-sch%C3%B6nsten-Sagen-des-klassischen-Altertums",
                        bild: "https://www.morawa.at/annotstream/9783785582756/COP/Schwab-Gustav/Die-sch%C3%B6nsten-Sagen-des-klassischen-Altertums.jpg?sq=4",
                        priority: 4,
                        zeiteinheiten: 26,
                    },
                    {
                        name: "Illias / Odyssee",
                        seiten: 880,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1050484856",
                        bild: "https://m.media-amazon.com/images/I/51nT9+dADfL.jpg",
                        priority: 2,
                        zeiteinheiten: 40,
                    },
                ],
                Filme: [
                    {
                        name: "Percy Jackson - Diebe im Olymp",
                        dauer: 113,
                        url: "https://www.amazon.de/Troja-Brad-Pitt/dp/B000F2C6M8",
                        bild: "https://m.media-amazon.com/images/I/610aQwwkYWL.jpg",
                        priority: 9,
                        zeiteinheiten: 120,
                    },
                    {
                        name: "Troja",
                        dauer: 156,
                        url: "https://www.amazon.de/Percy-Jackson-Diebe-im-Olymp/dp/B003H4X008",
                        bild: "https://m.media-amazon.com/images/I/71dArvWD4jL._SL1200_.jpg",
                        priority: 8,
                        zeiteinheiten: 160,
                    },
                    {
                        name: "Kampf der Titanen",
                        dauer: 106,
                        url: "https://www.netflix.com/watch/70119812?trackId=255824129&tctx=0%2C1%2CNAPA%40%40%7C6929e999-9404-4b5e-b858-7e24ee92e05e-239691146_titles%2F1%2F%2Fkrieg%20der%20g%C3%B6tter%2F0%2F0%2CNAPA%40%40%7C6929e999-9404-4b5e-b858-7e24ee92e05e-239691146_titles%2F1%2F%2Fkrieg%20der%20g%C3%B6tter%2F0%2F0%2Cunknown%2C%2C6929e999-9404-4b5e-b858-7e24ee92e05e-239691146%7C1%2CtitlesResults%2C70119812",
                        bild: "https://static.kino.de/wp-content/uploads/2015/08/kampf-der-titanen-2010-filmplakat.jpg",
                        priority: 9,
                        zeiteinheiten: 110,
                    },
                ],
                Artikel: [
                    {
                        name: "What is a greek myth?",
                        dauer: 90,
                        url: "https://pure.rug.nl/ws/portalfiles/portal/3385034/c1.pdf",
                        bild: "https://images.tandf.co.uk/common/jackets/crclarge/978041574/9780415744522.jpg",
                        priority: 2,
                        zeiteinheiten: 15,
                    },
                    {
                        name: "Sprachauffassungen im frühgriechischen Epos und in der griechischen Mythologie",
                        dauer: 70,
                        url: "https://archiv.ub.uni-heidelberg.de/propylaeumdok/3914/1/Liebermann_Sprachauffassungen_1996.pdf",
                        bild: "http://archiv.ub.uni-heidelberg.de/propylaeumdok/3914/1.haspreviewThumbnailVersion/Liebermann_Sprachauffassungen_1996.pdf",
                        priority: 4,
                        zeiteinheiten: 12,
                    },
                    {
                        name: "Jahresberichte: Griechische Mythologie",
                        dauer: 40,
                        url: "https://www.degruyter.com/document/doi/10.1524/phil.1859.14.14.113/pdf?casa_token=FmXBQ8EFwYQAAAAA:vwIdKbYsxk7XD02mmuhcR6j8TJ5Mw_nwggHTn96Hp5ybc5v-NxCAsnTBUt4YZZ4VL6ZQC5bg4jz-",
                        bild: "https://www.persee.fr/renderIssueCoverThumbnail/reg_0035-2039_1910_num_23_105.jpg",
                        priority: 4,
                        zeiteinheiten: 8,
                    },
                ],
                id: 1,
            },
            //_______________________________________________
            {
                name: "Peaky Blinders",
                Buecher: [
                    {
                        name: "Peaky Blinders - The Real Story",
                        seiten: 288,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1055060234?ProvID=11010473&gclid=CjwKCAjw4ayUBhA4EiwATWyBrjyX-sF3hqDGQ0p2Qtsqeje9fgF2zAnw0ZrR77rSoQP6PGezFFyhiBoC3IgQAvD_BwE",
                        bild: "https://assets.thalia.media/img/artikel/c56b6a0635bcaba55089aab0ffc2a3688dbe7efb-00-00.jpeg",
                        priority: 3,
                        zeiteinheiten: 20,
                    },
                    {
                        name: "The Gangs of Birmingham",
                        seiten: 302,
                        url: "https://www.amazon.de/Gangs-Birmingham-Philip-Gooderson/dp/1903854881",
                        bild: "https://images-eu.ssl-images-amazon.com/images/I/51TJaOUL-XL._SY291_BO1,204,203,200_QL40_ML2_.jpg",
                        priority: 4,
                        zeiteinheiten: 25,
                    },
                    {
                        name: "By Order of the Peaky Blinders: The Official Companion to the Hit TV Series",
                        seiten: 224,
                        url: "https://www.amazon.de/Order-Peaky-Blinders-Matt-Allen/dp/1789291658",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/51-PN7gkJLL._SX385_BO1,204,203,200_.jpg",
                        priority: 4,
                        zeiteinheiten: 20,
                    },
                ],
                Filme: [
                    {
                        name: "watch Peaky Blinders",
                        dauer: 1368,
                        url: "https://www.bbc.co.uk/iplayer/episodes/b045fz8r/peaky-blinders",
                        bild: "https://deadline.com/wp-content/uploads/2018/03/pb4_master_keyart_aw_land_v2-_-35-e1584361674557.jpg",
                        priority: 7,
                        zeiteinheiten: 30,
                    },
                ],
                Artikel: [
                    {
                        name: "Die wahre Geschichte der Peaky Blinders",
                        dauer: 15,
                        url: "https://dernostalgiker.at/die-wahre-geschichte-der-peaky-blinders/",
                        bild: "https://dernostalgiker.at/wp-content/uploads/2020/09/nintchdbpict000367059769.jpg",
                        priority: 3,
                        zeiteinheiten: 15,
                    },
                ],
                id: 2,
            },
            //_______________________________________________
            {
                name: "Marco Polo",
                Buecher: [
                    {
                        name: "Die wunderbaren Reisen des Marco Polo",
                        seiten: 112,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1007958715?ProvID=11010473&gclid=Cj0KCQjwvqeUBhCBARIsAOdt45ZWKArY9KpTjC8eSwqaywLn-VK77916yePqYcTbCyu4gsFLXt0UfT8aAqNxEALw_wcB",
                        bild: "https://assets.thalia.media/img/artikel/fee9850ea1e9c6e4d71d675e47e302212b736ea0-00-00.jpeg",
                        priority: 7,
                        zeiteinheiten: 15,
                    },
                    {
                        name: "The Book of Ser Marco Polo, the Venetian",
                        seiten: 570,
                        url: "https://www.medimops.de/henry-yule-the-book-of-ser-marco-polo-the-venetian-2-volume-set-the-book-of-ser-marco-polo-the-venetian-concerning-the-kingdoms-and-marvels-of-the-east-collection-travel-and-exploration-in-asia-taschenbuch-M01108022065.html?kk=a4c6295-180ed780add-a59c0&gclid=Cj0KCQjwvqeUBhCBARIsAOdt45Z8vCPcoERJ3E70Y889PtbvA6aNyAI_7Wdx4iXi4U6DHnD_lQXOOq4aAgvpEALw_wcB&variant=LibriNew&utm_source=PSM_KOO&utm_medium=cpc",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/51SL0TPdnfL._SX322_BO1,204,203,200_.jpg",
                        priority: 3,
                        zeiteinheiten: 50,
                    },
                    {
                        name: "Il Milione - Die Wunder der Welt",
                        seiten: 500,
                        url: "https://www.amazon.de/Milione-Die-Wunder-Welt/dp/3717516469",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/41uENaPeV0L.jpg",
                        priority: 4,
                        zeiteinheiten: 45,
                    },
                    {
                        name: "Marco Polo: Geschichte eines Entdeckers",
                        seiten: 112,
                        url: "https://www.geo.de/geolino/mensch/14677-rtkl-geschichte-eines-entdeckers-marco-polos-reise-nach-china",
                        bild: "https://image.geo.de/30112890/t/Bf/v3/w960/r0/-/marco-polo-reitet-mit-karawane-durch-die-wueste-in-der-hitze-und-sonne-jpg--62566-.jpg",
                        priority: 4,
                        zeiteinheiten: 10,
                    },
                ],
                Filme: [
                    {
                        name: "Marco Polo - Entdecker oder Lügner?",
                        dauer: 51,
                        url: "https://www.amazon.de/Marco-Polo-Entdecker-oder-L%C3%BCgner/dp/B01MSPJL25/ref=sr_1_1?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3L6HEMLD9RW1P&keywords=marco+polo+doku&qid=1653250796&sprefix=marco+polo+doku%2Caps%2C106&sr=8-1",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/A1A-BDv7OYL._SX300_.jpg",
                        priority: 7,
                        zeiteinheiten: 60,
                    },
                ],
                Artikel: [
                    {
                        name: "Marco Polo - Ein Leben zwischen Orient und Okzident",
                        dauer: 150,
                        url: "https://docplayer.org/10198722-Marco-polo-ein-leben-zwischen-orient-und-okzident.html",
                        bild: "https://media.springernature.com/w153/springer-static/cover/book/978-3-642-24337-0.jpg",
                        priority: 5,
                        zeiteinheiten: 14,
                    },
                ],
                id: 3,
            },
            //_______________________________________________
            {
                name: "Bitcoin",
                Buecher: [
                    {
                        name: "Bitcoin & Blockchain - Grundlagen und Programmierung",
                        seiten: 412,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1048330309",
                        bild: "https://assets.thalia.media/img/artikel/f59264d0941cc4eb024a9b7ac8bb6fd065cf9375-00-00.jpeg",
                        priority: 1,
                        zeiteinheiten: 30,
                    },
                    {
                        name: "BITCOIN - Geld ohne Staat",
                        seiten: 208,
                        url: "https://www.amazon.de/BITCOIN-digitale-W%C3%A4hrung-Wiener-Volkswirtschaft/dp/3898799115",
                        bild: "https://images-eu.ssl-images-amazon.com/images/I/51yFBP4uvnL._SY264_BO1,204,203,200_QL40_ML2_.jpg",
                        priority: 5,
                        zeiteinheiten: 20,
                    },
                    {
                        name: "Der Bitcoin-Standard",
                        seiten: 432,
                        url: "https://www.amazon.de/Bitcoin-Standard-Die-dezentrale-Alternative-Zentralbankensystem/dp/3982109507",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/31CmJEmi5zL._SX394_BO1,204,203,200_.jpg",
                        priority: 3,
                        zeiteinheiten: 30,
                    },
                ],
                Filme: [
                    {
                        name: "Banking on Bitcoin | BITCOIN DOCUMENTARY",
                        dauer: 83,
                        url: "https://www.youtube.com/watch?v=ByvbAi924TI",
                        bild: "https://i.ytimg.com/vi/ICNBX1vkwMk/maxresdefault.jpg",
                        priority: 6,
                        zeiteinheiten: 40,
                    },
                ],
                Artikel: [
                    {
                        name: "Was ist Bitcoin?",
                        dauer: 40,
                        url: "https://www.cryptolist.de/bitcoin",
                        bild: "https://www.cryptolist.de/img/article/krypto_tauschen.jpg",
                        priority: 4,
                        zeiteinheiten: 10,
                    },
                ],
                id: 4,
            },
            //_______________________________________________
            {
                name: "Marie Curie",
                Buecher: [
                    {
                        name: "Die Radioaktivität",
                        seiten: 1038,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1055060234?ProvID=11010473&gclid=CjwKCAjw4ayUBhA4EiwATWyBrjyX-sF3hqDGQ0p2Qtsqeje9fgF2zAnw0ZrR77rSoQP6PGezFFyhiBoC3IgQAvD_BwE",
                        bild: "https://books.google.at/books/content?id=5vrTdBc3I1UC&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE72ZVwLqiAIg672RidRghGNBVCWK6ZrppMGmS-8ePJe9JnwyaHc_sq5rbxkeeOSggd-Plpv19YDEyvoET9aKzdYO_cnZRh9NyT1ZDIMIKQ-LgPZUsHBg9JiyjODuzKhsvmU2m1H6",
                        priority: 1,
                        zeiteinheiten: 60,
                    },
                    {
                        name: "Radioactive Substances (Great Minds)",
                        seiten: 122,
                        url: "https://www.amazon.de/Radioactive-Substances-Great-Minds-Marie/dp/1573929573",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/41mOmywHC+L._SX309_BO1,204,203,200_.jpg",
                        priority: 3,
                        zeiteinheiten: 12,
                    },
                    {
                        name: "Die Entdeckung des Radiums",
                        seiten: 186,
                        url: "https://www.thalia.at/shop/home/artikeldetails/A1055060234?ProvID=11010473&gclid=CjwKCAjw4ayUBhA4EiwATWyBrjyX-sF3hqDGQ0p2Qtsqeje9fgF2zAnw0ZrR77rSoQP6PGezFFyhiBoC3IgQAvD_BwE",
                        bild: "https://images-na.ssl-images-amazon.com/images/I/41xPL2ja0uL.jpg",
                        priority: 2,
                        zeiteinheiten: 28,
                    },
                ],
                Filme: [
                    {
                        name: "Marie Curie – Elemente des Lebens",
                        dauer: 103,
                        url: "https://www.youtube.com/watch?v=LmNGuJugzn8",
                        bild: "https://m.media-amazon.com/images/I/41Ait9NGM2L.jpg",
                        priority: 7,
                        zeiteinheiten: 50,
                    },
                ],
                Artikel: [
                    {
                        name: "Weltveränderer Marie Curie",
                        dauer: 10,
                        url: "https://www.geo.de/geolino/mensch/3420-rtkl-weltveraenderer-marie-curie",
                        bild: "https://image.geo.de/30089414/t/41/v3/w960/r0/-/marie-curie-gross-jpg--43489-.jpg",
                        priority: 3,
                        zeiteinheiten: 10,
                    },
                ],
                id: 5,
            },
        ];
    }

    topics: any;

    public get topicList(): string {
        return this.topics;
    }
}
