//Array of Questions, user in ContentOverviewScreen
export class Questions {
  constructor() {
    this.topics = [
      {
        topicId: 1, questions: //Griechishce Mythologie_________________________________________________________
          [{
            questionId: 0,
            question: "Wer gehört NICHT zu den 12 olympischen Göttern?",
            answers: [
              { id: "1", text: "Poseidon" },
              { id: "2", text: "Hades", correct: true },
              { id: "3", text: "Hermes" },
              { id: "4", text: "Aphrodite" }
            ],
            correct: true
          },
          {
            questionId: 1,
            question: "Wann entstanden ca. Hmoers Ilias und Odyssee?",
            answers: [
              { id: "1", text: "ca. 2000 Jahre v. Chr." },
              { id: "2", text: "ca. 1200 Jahre v. Chr." },
              { id: "3", text: "ca. 100 Jahre n. Chr." },
              { id: "4", text: "ca. 700 Jahre v. Chr.", correct: true }
            ],
            correct: true
          },
          {
            questionId: 2,
            question: "Was haben Ares und Athena NICHT gemeinsam?",
            answers: [
              { id: "1", text: "Gott/Göttin des Kampfes" },
              { id: "2", text: "Kind von Zeus" },
              { id: "3", text: "ölympische Göttin / olympischer Gott", correct: true },
              { id: "4", text: "Stadt nach Gott/Göttin benannt" }
            ],
            correct: false
          },
          {
            questionId: 3,
            question: "Wie lange Dauert die Reise von Odysseus?",
            answers: [
              { id: "1", text: "3 Jahre" },
              { id: "2", text: "10 Jahre", correct: true },
              { id: "3", text: "17 Jahre und 1 Tag" },
              { id: "4", text: "20 Jahre" }
            ],
            correct: false
          },
          {
            questionId: 4,
            question: "Warum war Achilles an seiner Ferse verletzlich?",
            answers: [
              { id: "1", text: "Beim trocknen fiel ein Blatt auf seine Ferse" },
              { id: "2", text: "Halbgötter dürfen nie unverwundbar sein" },
              { id: "3", text: "Seine Mutter (Thetis) wollte dass Achilles menschlich bleibt" },
              { id: "4", text: "Seine Mutter (Thetis) hat Achilles beim tauchen ins Wasser an seiner Ferse gehalten", correct: true }
            ],
            correct: false
          },
          {
            questionId: 5,
            question: "Zeus und Hera haben 4 Kinder, wer gehört nicht dazu?",
            answers: [
              { id: "1", text: "Apollo", correct: true },
              { id: "2", text: "Ares" },
              { id: "3", text: "Hebe" },
              { id: "4", text: "Hephaistos" }
            ],
            correct: false
          },
          {
            questionId: 6,
            question: "Zeus hat 5 Geschwister, wer gehört nicht dazu?",
            answers: [
              { id: "1", text: "Hera" },
              { id: "2", text: "Leto", correct: true },
              { id: "3", text: "Hestia" },
              { id: "4", text: "Poseidon" }
            ],
            correct: false
          },
          {
            questionId: 7,
            question: "Meduse, zuvor 'wunderschön' wurde von Athene in ein Ungeheuer verwandelt, warum?",
            answers: [
              { id: "1", text: "Athene wollte die schönste Frau Athens sein, Medusa stand ihr im Weg" },
              { id: "2", text: "Medusa hatte Geschlechtsverkehr mit Poseidon", correct: true },
              { id: "3", text: "Medusa verführte eines von Athenes Kindern" },
              { id: "4", text: "Athene war langweilig" }
            ],
            correct: false
          },
          {
            questionId: 8,
            question: "Zeus hatte kein Verhältnis mit ...",
            answers: [
              { id: "1", text: "einer Schwester" },
              { id: "2", text: "seiner Mutter" },
              { id: "3", text: "einem Mann" },
              { id: "4", text: "Zeus hatte ein Verhältnis mit allen der obrigen", correct: true }
            ],
            correct: false
          },
          {
            questionId: 9,
            question: "Athene hat 2 Schutzgöttinen/Schutzgötter, Athene uns ...",
            answers: [
              { id: "1", text: "Hermes" },
              { id: "2", text: "Aphrodite" },
              { id: "3", text: "Zeus" },
              { id: "4", text: "Poseidon", correct: true }
            ],
            correct: false
          }]
      },
      {
        topicId: 2, questions: //Peaky Blinders_________________________________________________________
          [{
            questionId: 0,
            question: "Welcher politischen Partei gehört Tommy offiziell an, wenn er Abgeordneter wird?",
            answers: [
              { id: "1", text: "Labour Party", correct: true },
              { id: "2", text: "Green Party" },
              { id: "3", text: "Conservative Party" },
              { id: "4", text: "Liberals Party" }
            ],
            correct: false
          },
          {
            questionId: 1,
            question: "Wie lautet der offizielle Name des Familienunternehmens der Peaky Blinders?",
            answers: [
              { id: "1", text: "Shelby Company Limited", correct: true },
              { id: "2", text: "Shelby GmbH" },
              { id: "3", text: "Shelby Company Public Company Limited" },
              { id: "4", text: "Shelby s.r.o." }
            ],
            correct: false
          },
          {
            questionId: 2,
            question: "Wie lautete der Name der von Oswald Mosely gegründeten politischen Partei?",
            answers: [
              { id: "1", text: "The Coalition Union of Fascists" },
              { id: "2", text: "The British Union of Fascists", correct: true },
              { id: "3", text: "The Empire’s Union of Fascists" },
              { id: "4", text: "The United Kingdom’s Union of Fascists" }
            ],
            correct: false
          },
          {
            questionId: 3,
            question: "In der ersten Staffel dreht sich alles um gestohlene Waffen. Wohin sollten sie denn gehen?",
            answers: [
              { id: "1", text: "Nordirland" },
              { id: "2", text: "Indien" },
              { id: "3", text: "Libyen", correct: true },
              { id: "4", text: "USA" }
            ],
            correct: false
          },
          {
            questionId: 4,
            question: "Von welchem Gegenstand leitet sich der Name “Peaky Blinders” ab?",
            answers: [
              { id: "1", text: "Taschenmesser" },
              { id: "2", text: "Korkenzieher" },
              { id: "3", text: "Gabel" },
              { id: "4", text: "Rasierklingenkappen", correct: true }
            ],
            correct: false
          },
          {
            questionId: 5,
            question: "Aus welcher Situation sind die “Peaky Blinders” entstanden?",
            answers: [
              { id: "1", text: "wirtschaftlicher Not", correct: true },
              { id: "2", text: "politischer Not" },
              { id: "3", text: "religiöser Not" },
              { id: "4", text: "sozialer Not" }
            ],
            correct: false
          },
          {
            questionId: 6,
            question: "Zu welcher Zeit spielte die Serie “Peaky Blinders” in Grossbritannien?",
            answers: [
              { id: "1", text: "Nach dem Zweiten Weltkrieg" },
              { id: "2", text: "Während dem Kolonialismus" },
              { id: "3", text: "Nach dem Ersten Weltkrieg", correct: true },
              { id: "4", text: "Vor dem Fall der Berliner Mauer" }
            ],
            correct: false
          },
          {
            questionId: 7,
            question: "Die Irish Republican Army kaufte welche Gegenstände für welches Ereignis?",
            answers: [
              { id: "1", text: "Diamanten für Selbstzwecke" },
              { id: "2", text: "Waffen und Munition für den Unabhängigkeitskrieg", correct: true },
              { id: "3", text: "Waffen und Munition für die Polizeiakademie" },
              { id: "4", text: "Waffen und Munition für die Jagd" }
            ],
            correct: false
          },
          {
            questionId: 8,
            question: "Welche politische Ideologie verbreitete sich stark in Grossbritannien nach dem Ersten Weltkrieg?",
            answers: [
              { id: "1", text: "Die Grünen" },
              { id: "2", text: "Die Liberalen" },
              { id: "3", text: "Die Idealisten" },
              { id: "4", text: "Die Kommunisten", correct: true }
            ],
            correct: false
          },
          {
            questionId: 9,
            question: "Aus welcher Stadt kommen die “Peaky Blinders”",
            answers: [
              { id: "1", text: "London" },
              { id: "2", text: "Edinburgh" },
              { id: "3", text: "Birmingham", correct: true },
              { id: "4", text: "Liverpool " }
            ],
            correct: false
          }]
      },
      {
        topicId: 3, questions: //Marco Polo_________________________________________________________
          [{
            questionId: 0,
            question: "In welcher italienischen Stadt soll Marco Polo angeblich geboren worden sein?",
            answers: [
              { id: "1", text: "Rom" },
              { id: "2", text: "Mailand" },
              { id: "3", text: "Venedig", correct: true },
              { id: "4", text: "Lecce" }
            ],
            correct: false
          },
          {
            questionId: 1,
            question: "Wie lauteten die Namen von Marcos Vater und Onkel?",
            answers: [
              { id: "1", text: "Maffeo und Niccolo", correct: true },
              { id: "2", text: "Julius und Niccolo" },
              { id: "3", text: "Maffeo und Julius" },
              { id: "4", text: "Maffeo und Marco" }
            ],
            correct: false
          },
          {
            questionId: 2,
            question: "In welchem Jahr reisten Marcos Vater und Onkel zum ersten Mal nach China?",
            answers: [
              { id: "1", text: "1260", correct: true },
              { id: "2", text: "1268" },
              { id: "3", text: "1271" },
              { id: "4", text: "1270" }
            ],
            correct: false
          },
          {
            questionId: 3,
            question: "In welchem Jahr brach Marco nach China auf?",
            answers: [
              { id: "1", text: "1266" },
              { id: "2", text: "1271", correct: true },
              { id: "3", text: "1283" },
              { id: "4", text: "1277" }
            ],
            correct: false
          },
          {
            questionId: 4,
            question: "Aus welchem Land stammte der Khan von China?",
            answers: [
              { id: "1", text: "China" },
              { id: "2", text: "Russland" },
              { id: "3", text: "Persien" },
              { id: "4", text: "Mongolien", correct: true }
            ],
            correct: false
          },
          {
            questionId: 5,
            question: "Wer war die Prinzessin, die Marco und sein Vater und Onkel am Ende ihrer Zeit in China nach Persien brachten?",
            answers: [
              { id: "1", text: "Bathtina" },
              { id: "2", text: "Empacea" },
              { id: "3", text: "Hereelda" },
              { id: "4", text: "Cocachin", correct: true }
            ],
            correct: false
          },
          {
            questionId: 6,
            question: "Während des Krieges zwischen welchen beiden Städten saß Marco im Gefängnis?",
            answers: [
              { id: "1", text: "Venedig und Bari" },
              { id: "2", text: "Venedig und Petropavlovsk-Kamchatskiy" },
              { id: "3", text: "Venedig und Genua", correct: true },
              { id: "4", text: "Venedig und Marseille" }
            ],
            correct: false
          },
          {
            questionId: 7,
            question: "In welchem Jahr wurde das Buch von Marco fertiggestellt?",
            answers: [
              { id: "1", text: "1295" },
              { id: "2", text: "1299", correct: true },
              { id: "3", text: "1300" },
              { id: "4", text: "1302" }
            ],
            correct: false
          },
          {
            questionId: 8,
            question: "Was inspirierte die Polos zu ihrer Reise über die Seidenstraße nach China?",
            answers: [
              { id: "1", text: "Sie wollten unbedingt gebratenen Reis probieren" },
              { id: "2", text: "Sie interessierten sich für die Ländereien, die die Waren produzierten, mit denen sie handelten", correct: true },
              { id: "3", text: "Sie erhielten eine persönliche Einladung von Kublai Khan" },
              { id: "4", text: "Sie wollten die Seide direkt in China kaufen und zu Hause in Venedig verkaufen" }
            ],
            correct: false
          },
          {
            questionId: 9,
            question: "Wer war Kublai Khan?",
            answers: [
              { id: "1", text: "Marco’s Onkel" },
              { id: "2", text: "Italienischer Kaiser" },
              { id: "3", text: "Chinesischer Kaiser", correct: true },
              { id: "4", text: "Marco’s indischer Übersetzer" }
            ],
            correct: false
          }]
      },
      {
        topicId: 4, questions: //Bitcoin_________________________________________________________
          [{
            questionId: 0,
            question: "Bitcoin wurde erstmals veröffentlicht im Jahre?",
            answers: [
              { id: "1", text: "2004" },
              { id: "2", text: "2008" },
              { id: "3", text: "2009", correct: true },
              { id: "4", text: "2011" }
            ],
            correct: false
          },
          {
            questionId: 1, 
            question: "Was passiert bei Crypto-Arbitrage?",
            answers: [
              { id: "1", text: "Viele AnlegerInnen schließen sich zum gebündelten Kauf von Coins zusammen" },
              { id: "2", text: "Hacker nutzen eine Sicherheitslücke, um Cold Wallets anzugreifen" },
              { id: "3", text: "Preisunterschiede auf verschiedenen Trading-Plattformen werden ausgenutzt", correct: true },
              { id: "4", text: "Die gekauften Cryptos verlieren oder gewinnen stark an Wert" }
            ],
            correct: false
          },
          {
            questionId: 2,
            question: "Krypto-Wallets sind anfällig für Hackerangriffe. Bei welchem Angriff werden die infizierten Computer als Miner genutzt?",
            answers: [
              { id: "1", text: "Reverse-Proxy-Phishing" },
              { id: "2", text: "Kryptojacking", correct: true },
              { id: "3", text: "Clipping" },
              { id: "4", text: "Teardrop-Angriff" }
            ],
            correct: false
          },
          {
            questionId: 3,
            question: "Was ist die Bitcoin-Blockchain nicht?",
            answers: [
              { id: "1", text: "quelloffen" },
              { id: "2", text: "skalierbar", correct: true },
              { id: "3", text: "dezentral" },
              { id: "4", text: "gar nichts" }
            ],
            correct: false
          },
          {
            questionId: 4,
            question: "Die Menge der maximal zur Verfügung stehenden Bitcoin jemals liegt bei?",
            answers: [
              { id: "1", text: "21.000.000$", correct: true },
              { id: "2", text: "210.000.000$" },
              { id: "3", text: "1.000.000$" },
              { id: "4", text: "unlimitiert" }
            ],
            correct: false
          },
          {
            questionId: 5,
            question: "Bitcoin wurde erfunden von?",
            answers: [
              { id: "1", text: "Der US-Regierung und der Chinesischen Regierung" },
              { id: "2", text: "Elon Musk" },
              { id: "3", text: "Eine anonyme Person oder Gruppe, die das Pseudonym von Satoshi Nakamoto angenommen hat", correct: true },
              { id: "4", text: "Forscher von Harvard" }
            ],
            correct: false
          },
          {
            questionId: 6,
            question: "Wie wird der Wert eines Bitcoin bestimmt?",
            answers: [
              { id: "1", text: "Durch einen mit Bitcoin selbst erfundenen Algorithmus" },
              { id: "2", text: "Durch das Angebot und die Nachfrage bei Bitcoin-Börsen und Menschen, die Bitcoin kaufen und verkaufen", correct: true },
              { id: "3", text: "Von der US-Regierung" },
              { id: "4", text: "Von der Gruppe von Menschen hinter Satoshi Nakamoto" }
            ],
            correct: false
          },
          {
            questionId: 7,
            question: "Wie sieht wie eine Bitcoin-Adresse aus?",
            answers: [
              { id: "1", text: "abd3nd7dkdfs7s8djnfs7shdfnn4i7s9sjfs989whfnueh8d", correct: true },
              { id: "2", text: "===_______=====-----==--__--_=-" },
              { id: "3", text: "bitcoin.com/arealme.address.btc/1999" },
              { id: "4", text: "19273467291634838928349826362" }
            ],
            correct: false
          },
          {
            questionId: 8,
            question: "Was ist ein Seed?",
            answers: [
              { id: "1", text: "Eine oft leicht zu merkende Wortfolge, die den Privat Key regenerieren kann", correct: true },
              { id: "2", text: "Ein Alias-Name für deinen Private Key" },
              { id: "3", text: "Ein Alias-Name für dein Passwort" },
              { id: "4", text: "Eine Folge von zufälligen Wörtern, die oft leicht zu merken sind und von deinem Private Key erzeugt werden" }
            ],
            correct: false
          },
          {
            questionId: 9,
            question: "Die erste Transaktion mit physischen Gütern wurde am 22. Mai 2010 bezahlt mit 10.000 BTC im Tausch gegen?",
            answers: [
              { id: "1", text: "zwei Pizzen", correct: true },
              { id: "2", text: "einen BMW X3" },
              { id: "3", text: "Ein Grundstück in Florida" },
              { id: "4", text: "Ein paar Sneakers" }
            ],
            correct: false
          }]
      },
      {
        topicId: 5, questions: //Marie Curie_________________________________________________________
          [{
            questionId: 0,
            question: "Wie heißt ein von Marie Curie entdecktes Element, welches sie nach ihrem Heimatland benannt hat?",
            answers: [
              { id: "1", text: "Britanium" },
              { id: "2", text: "Polnium ", correct: true },
              { id: "3", text: "Francium" },
              { id: "4", text: "Germanium" }
            ],
            correct: false
          },
          {
            questionId: 1,
            question: "Welchem Thema widmete sie ihre Doktorarbeit?",
            answers: [
              { id: "1", text: "Der von Einstein aufgestellten Relativitätstheorie" },
              { id: "2", text: "Untersuchung der von Becquerel entdeckten Strahlung", correct: true },
              { id: "3", text: "Untersuchung der X-Strahlung" },
              { id: "4", text: "Der elektrischen Leitfähigkeit" }
            ],
            correct: false
          },
          {
            questionId: 2,
            question: "Wo wurde Marie Curie geboren?",
            answers: [
              { id: "1", text: "Moskau" },
              { id: "2", text: "Warschau ", correct: true },
              { id: "3", text: "Marseille" },
              { id: "4", text: "Paris" }
            ],
            correct: false
          },
          {
            questionId: 3,
            question: "Wie viele Töchter hatte Marie Curie?",
            answers: [
              { id: "1", text: "2", correct: true },
              { id: "2", text: "4" },
              { id: "3", text: "1" },
              { id: "4", text: "3" }
            ],
            correct: false
          },
          {
            questionId: 4,
            question: "Wann gewann sie ihren ersten Nobel Prize?",
            answers: [
              { id: "1", text: "1904" },
              { id: "2", text: "1911" },
              { id: "3", text: "1903", correct: true },
              { id: "4", text: "1905" }
            ],
            correct: false
          },
          {
            questionId: 5,
            question: "Welche 2 Elemente hat Marie Curie entdeckt?",
            answers: [
              { id: "1", text: "Radioaktivität, Polonium" },
              { id: "2", text: "Radium, Blei" },
              { id: "3", text: "Uran, Radium" },
              { id: "4", text: "Radium, Polonium", correct: true }
            ],
            correct: false
          },
          {
            questionId: 6,
            question: "Wie wurden die tragbaren Röntgengeräte genannt, welche von Marie Curie erfunden wurden?",
            answers: [
              { id: "1", text: "small x-rays" },
              { id: "2", text: "little curies", correct: true },
              { id: "3", text: "tiny curies" },
              { id: "4", text: "x-ray ambulances" }
            ],
            correct: false
          },
          {
            questionId: 7,
            question: "Marie Curie hat studiert, wie Radium was heilen kann?",
            answers: [
              { id: "1", text: "Diabetis" },
              { id: "2", text: "Lungenschäden" },
              { id: "3", text: "gewöhnliche Erkältungen" },
              { id: "4", text: "Krebs", correct: true }
            ],
            correct: false
          },
          {
            questionId: 8,
            question: "In welchen Gebieten wurde ihr der Nobelpreis verliehen?",
            answers: [
              { id: "1", text: "Physik & Mathematik" },
              { id: "2", text: "Physik & Chemie", correct: true },
              { id: "3", text: "Chemie & Mathematik" },
              { id: "4", text: "Biologie & Chemie" }
            ],
            correct: false
          },
          {
            questionId: 9,
            question: "Woran starb Marie Curie?",
            answers: [
              { id: "1", text: "Leukämie", correct: true },
              { id: "2", text: "Tuberkulose" },
              { id: "3", text: "An den Folgen eines Unfalls" },
              { id: "4", text: "Altersschwäche" }
            ],
            correct: false
          }]
      }];
  }
  topics: any;
}