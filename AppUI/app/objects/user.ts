// User Object, gets passed from screen to screen and safes information about
// personalisation, started/finished content, ...
// id gets inserted, when user selected this topic

export class User {
    constructor() {
        this.userName = ""
        this.password = ""
        this.topics = [
            {
                id: null, name: "Griechische Mythologie", priority: 0, time: 0,
                finished: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }],
                started: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }]
            },

            {
                id: null, name: "Peaky Blinders", priority: 0, time: 0,
                finished: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }],
                started: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }]
            },

            {
                id: null, name: "Marco Polo", priority: 0, time: 0,
                finished: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }],
                started: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }]
            },

            {
                id: null, name: "Bitcoin", priority: 0, time: 0,
                finished: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }],
                started: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }]
            },

            {
                id: null, name: "Marie Curie", priority: 0, time: 0,
                finished: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }],
                started: [{ Buecher: [] }, { Filme: [] }, { Artikel: [] }]
            }
        ]
    }

    password: string;
    userName: string
    topics: any
}