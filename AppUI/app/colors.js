export const colors = {
    fehler: "#F4976C", // orange
    warning: "#FBE8A6", // yellow
    blueDark: "#303C6C",
    blueLight1: "#B4DFE5",
    blueLight2: "#D2FDFF",
    titleBlue: "#6Fa4AB",
    quizGreen: "#6FAB7A",
    quizRed: "#E07575"
};