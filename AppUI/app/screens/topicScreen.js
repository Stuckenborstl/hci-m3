import * as React from "react";
import {
    StatusBar,
    View,
    StyleSheet,
    FlatList,
    Text,
    SafeAreaView,
    Alert,
    TouchableOpacity,
    Button,
} from "react-native";
import { SearchBar, ListItem } from "react-native-elements";
import { Content } from "../objects/content.ts";
import styles from "../styles";
import colors from "../colors";

export default function TopicScreen({ navigation, route }) {
    //const TopicScreen = (navigation) => {
    //console.log(route.params);

    var c = new Content();
    //console.log(c.topics);
    //console.log(u.topics);
    var userInfo = global.user.topics;

    var source = c.topics; // data

    const [selectedId, setSelectedId] = React.useState(null);
    const [butVis, setButVis] = React.useState(false);
    const [search, setSearch] = React.useState("");
    const [filteredSource, setFilteredSource] = React.useState(source);
    const [origSource, setOrigSource] = React.useState(source);
    /*
    const itemBg = '#8fc1e3';
    const itemText = 'black';*/

    //setFilteredSource(source);

    //console.log('Screen opened with following source data: ');
    //console.log({source});
    //console.log({filteredSource});
    //console.log({origSource});

    const Item = ({ item, onPress, backgroundColor, textColor }) => (
        <TouchableOpacity
            onPress={onPress}
            style={[styleC.item, backgroundColor]}
        >
            <Text style={[styles.simpleText, textColor]}>{item.name}</Text>
        </TouchableOpacity>
    );

    const ItemBorder = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#31708e",
                }}
            />
        );
    };

    const renderHeader = () => {
        // search bar
        return (
            <SearchBar
                lightTheme
                cancelIcon
                round
                placeholder="Search for a topic"
                value={search}
                onChangeText={(input) => searchFilter(input)}
                onClear={(input) => searchFilter("")}
                autoCorrect={false}
            />
        );
    };

    const renderItem = ({ item }) => {
        const backgroundColor =
            item.id === selectedId ? "#6Fa4AB" : "transparent";
        const color = item.id === selectedId ? "white" : "black";

        return (
            <Item
                item={item}
                onPress={() => {
                    setSelectedId(item.id);
                    setButVis(true);
                }}
                backgroundColor={{ backgroundColor }}
                textColor={{ color }}
            />
        );
    };

    const searchFilter = (input) => {
        //console.log('searchFilter started.');
        if (input) {
            const newData = origSource.filter(function (item) {
                const itemUpper = item.name
                    ? item.name.toUpperCase()
                    : "".toUpperCase(); // convert titles into uppercase for easy comparison
                const inputUpper = input.toUpperCase();
                //console.log({input});
                return itemUpper.indexOf(inputUpper) > -1;
            });
            setFilteredSource(newData);
            setSearch(input);
        } else {
            // no input
            setFilteredSource(origSource);
            setSearch(input); // TODO try with ''
        }
    };
    // styleC.container
    return (
        <SafeAreaView style={styles.scrollViewFullW}>
            <Text style={styles.screenTitle}>Themen</Text>
            <Text style={styles.screenUntTitle}>Welches Thema interessiert dich?</Text>
            <SearchBar
                lightTheme
                cancelIcon
                round
                autoCorrect={false}
                onChangeText={(input) => searchFilter(input)}
                onClear={(input) => searchFilter("")}
                placeholder="Search for a topic"
                value={search}
            />
            <FlatList
                data={filteredSource}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
                extraData={selectedId}
                //ListHeaderComponent={renderHeader}
                ListEmptyComponent={<Text>No data</Text>}
                ItemSeparatorComponent={ItemBorder}
            />

            {butVis /* set to true to show, set to false to hide */ && (
                <Button
                    color="#F5834E"
                    //style={styles.submButt}
                    title="Weiter"
                    onPress={() => {
                        global.user.topics[selectedId - 1].id = selectedId;
                        navigation.navigate("Personalisierung", {
                            themaId: selectedId,
                        });
                    }}
                />
            )}
        </SafeAreaView>
    );
}

const styleC = StyleSheet.create({
    name: {
        padding: 5,
        fontSize: 20,
    },
    buttonCont: {
        right: 10,
        left: 10,
        position: "absolute",
        bottom: 10,
        backgroundColor: "#687864",
    },
});

//export default TopicScreen;

/* colors:
#687864
#31708e
#5085a5
#8fc1e3
#f7f9fb
*/
