import * as React from "react";
import { useState } from "react";
import { View, Text, ScrollView, Button } from "react-native";
import { BTMultiSelect } from "@blump-tech/native-base-select";
import { NativeBaseProvider } from "native-base";
import Slider from "react-native-slider";
import styles from "../styles";
import { colors } from "../colors";
import { Content } from "../objects/content";

//Gives personalisation-Options for given Topic
//All Topics personisationable individually
export default function PersonalisationScreen({ navigation, route }) {
    //Shows, if user tries to go to Content without Selecting a Topic first
    if (global.user.topics.find((t) => t.id !== null) === undefined)
        return (
            <View style={styles.centeredView}>
                <Text
                    style={{
                        fontSize: 24,
                        lineHeight: 36,
                        fontWeight: "bold",
                        textAlign: "center",
                        padding: 8,
                        color: colors.fehler,
                        textAlign: "center",
                    }}
                >
                    Bitte wähle zuerst ein Thema (unter Themen)
                </Text>
            </View>
        );

    var content = new Content(); //Object that contains Book-, Film- and Article- Objects to all available Topics
    var thema = content.topics.find((c) => c.id == route.params.themaId); //given by topicScreen

    const [time, setTime] = useState(20); //time-junk user has at once - 10min to 240 min
    const [purpose, setPurpose] = useState(5); //1 == learning, 9 == entertainment
    const [medium, setMedium] = useState({
        //list of all available Content Types
        value: "",
        selectedList: [],
        error: "",
        list: [
            { _id: 0, name: "Bücher" },
            { _id: 1, name: "Filme" },
            { _id: 2, name: "kurze Artikel" },
        ],
    });

    //return of personalisationScreen
    return (
        <ScrollView style={styles.scrollView}>
            <Text
                style={{
                    fontSize: 24,
                    fontWeight: "bold",
                    textAlign: "center",
                    paddingBottom: 10,
                    color: "#6Fa4AB",
                }}
            >
                Personalisierungen - {thema.name}
            </Text>
            <Text
                style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    textAlign: "center",
                    paddingBottom: 5,
                }}
            >
                Diese Fragen musst du beantworten, dann gehts los!
            </Text>
            <Text
                style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    textAlign: "center",
                }}
            >
                (Du kannst alles später noch ändern){" "}
            </Text>
            <View style={{ padding: 5 }}>
                <View style={{ flexDirection: "row" }}>
                    <Text
                        style={{
                            marginTop: 15,
                            fontSize: 14,
                            fontWeight: "bold",
                        }}
                    >
                        MedienTypen:{" "}
                    </Text>
                </View>
                <NativeBaseProvider>
                    <BTMultiSelect
                        placeholder="Wähle mindestens 2 Medien-Typen aus"
                        list={medium.list}
                        selectedList={medium.selectedList}
                        onSelection={(value) => {
                            setMedium({
                                ...medium,
                                value: value.text,
                                selectedList: value.selectedList,
                                error: "",
                                selectInputStyle: "paddingTop: 20",
                            });
                        }}
                        pillStyle={{
                            borderRadius: 5,
                            textColor: "black",
                            backgroundColor: "white",
                            fontWeight: "bold",
                        }}
                        selectInputStyle={{ backgroundColor: "#6Fa4AB" }}
                        placeHolderStyle={{
                            textColor: "white",
                            fontSize: 14,
                            fontWeight: "bold",
                        }}
                        errorText={medium.error}
                        errorStyle={{ textColor: "red" }}
                    />
                </NativeBaseProvider>
            </View>
            <View style={{ marginTop: 25, flexDirection: "row" }}>
                <Text style={{ fontSize: 14, fontWeight: "bold", padding: 5 }}>
                    Am wichtigsten ist mir:{" "}
                </Text>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        color: "white",
                        backgroundColor: "#6Fa4AB",
                        padding: 5,
                        borderRadius: 5,
                    }}
                >
                    {purpose < 5 ? "lernen" : "entertainment"}
                </Text>
            </View>
            <View style={{ padding: 5 }}>
                <Slider
                    value={purpose}
                    minimumValue={1}
                    maximumValue={9}
                    onValueChange={(value) => setPurpose(value)}
                />
            </View>
            <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 14, fontWeight: "bold", padding: 5 }}>
                    So viel Zeit habe ich auf einmal:{" "}
                </Text>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        color: "white",
                        backgroundColor: "#6Fa4AB",
                        padding: 5,
                        borderRadius: 5,
                    }}
                >
                    {time >= 60 ? parseInt(time / 60) + " h" : null}{" "}
                    {(time % 60) + " min"}
                </Text>
            </View>
            <View style={{ padding: 5 }}>
                <Slider
                    minimumValue={10}
                    maximumValue={240}
                    step={1}
                    value={time}
                    onValueChange={(value) => setTime(value)}
                />
            </View>
            <View style={{ marginRight: 120, marginLeft: 120, marginTop: 10 }}>
                <Button
                    title="Abgeben"
                    color="#F5834E"
                    onPress={() => {
                        global.user.topics.find(
                            (t) => t.name === thema.name
                        ).priority = purpose;
                        global.user.topics.find(
                            (t) => t.name === thema.name
                        ).time = time;
                        navigation.navigate("Inhalte", { themaId: thema.id });
                    }}
                />
            </View>
        </ScrollView>
    );
}
