import * as React from "react";
import {
    TextInput,
    View,
    Text,
    TouchableWithoutFeedback,
    Keyboard,
} from "react-native";
import { Button } from "react-native-elements";
import {
    FacebookSocialButton,
    GoogleSocialButton,
} from "react-native-social-buttons";
import styles from "./../styles";
import { AuthContext } from "../authContext";

//so fare, the LoginScreen is only used to set the Username and change the TabNavigation
export default function LoginScreen({ navigation }) {
    const [userName, setUserName] = React.useState(global.user.userName); //given Username
    const [password, setPassword] = React.useState(global.user.password); //given Password
    const [loggedIn, setLoggedIn] = React.useContext(AuthContext); //used to change Tab-Navigation (done in App.js)

    //return of LoginScreen
    //Login using Username, Facobook and Google possible
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.centeredView}>
                <Text
                    style={{
                        paddingTop: 30,
                        marginBottom: 10,
                        lineHeight: 30,
                        fontSize: 20,
                        fontWeight: "bold",
                        color: "#6Fa4AB",
                        textAlign: "center",
                        paddingBottom: 5,
                    }}
                >
                    Schön dass du da bist!{"\n"}Kurz einlogen und viel Spaß beim
                    lernen
                </Text>
                <TextInput
                    style={styles.input}
                    value={userName}
                    onChangeText={(text) => setUserName(text)}
                    placeholder="Username"
                ></TextInput>
                <TextInput
                    style={styles.inputDisabled}
                    editable={false}
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                    placeholder="Passwort"
                ></TextInput>
                <View style={styles.buttonView}>
                    <Button
                        title="Anmelden"
                        onPress={() => {
                            global.user.userName = userName;
                            global.user.password = password;
                            setLoggedIn(true);
                        }}
                    />
                </View>
                <View style={{ paddingTop: 20, paddingBottom: 10 }}>
                    <FacebookSocialButton
                        onPress={() => {
                            global.user.userName = "Facebook Account";
                            global.user.password = password;
                            setLoggedIn(true);
                        }}
                    />
                </View>
                <View>
                    <GoogleSocialButton
                        onPress={() => {
                            global.user.userName = "Google Account";
                            global.user.password = password;
                            setLoggedIn(true);
                        }}
                    />
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
}
