import * as React from "react";
import { ScrollView, Text, StyleSheet, View, Image, TouchableOpacity, Linking } from 'react-native';
import styles from './../styles';
import { Content } from './../objects/content';

export default function KilianScreen({ navigation, route }) {
    var content = new Content;
    var bookArray = content.topics.filter(t => t.name === global.user.userName)[0].Buecher;
    var filmArray = content.topics.filter(t => t.name === global.user.userName)[0].Filme;
    var artikelArray = content.topics.filter(t => t.name === global.user.userName)[0].Artikel;

    //As far as I am concerned, image-sources can not be required dynamically, so we have to use urls
    const showBooks = bookArray.map((book) => {
        return <View key={book.name} style={{ margin: 10, marginTop: 5, marginRight: 5, padding: 5, backgroundColor: "#D0F1F5", borderRadius: 5 }}>
            <TouchableOpacity onPress={() => Linking.openURL(book.url)}>
                <Text>{book.name}</Text>
                <View style={{ padding: 5, flexDirection: "row", paddingRight: 0 }}>
                    <Image source={{ uri: book.bild }} style={{ width: 80, height: 80 }} />
                    <View style={{ padding: 5, flexDirection: "column", justifyContent: "flex-end", paddingRight: 0 }}>
                        <Text>{book.seiten} Seiten</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    })

    const showFilms = filmArray.map((film) => {
        return <View key={film.name} style={{ margin: 10, marginTop: 5, marginRight: 5, padding: 5, backgroundColor: "#D0F1F5", borderRadius: 5 }}>
            <TouchableOpacity onPress={() => Linking.openURL(film.url)}>
                <Text>{film.name}</Text>
                <View style={{ padding: 5, flexDirection: "row", paddingRight: 0 }}>
                    <Image source={{ uri: film.bild }} style={{ width: 80, height: 80 }} />
                    <View style={{ padding: 5, flexDirection: "column", justifyContent: "flex-end", paddingRight: 0 }}>
                        <Text>{film.dauer >= 60 ? parseInt(film.dauer / 60) + "h" : null} {film.dauer % 60 + "min"}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    })

    const showArtikel = artikelArray.map((artikel) => {
        return <View key={artikel.name} style={{ margin: 10, marginTop: 5, marginRight: 5, padding: 5, backgroundColor: "#D0F1F5", borderRadius: 5 }}>
            <TouchableOpacity onPress={() => Linking.openURL(artikel.url)}>
                <Text>{artikel.name}</Text>
                <View style={{ padding: 5, flexDirection: "row", paddingRight: 0 }}>
                    <Image source={{ uri: artikel.bild }} style={{ width: 80, height: 80 }} />
                    <View style={{ padding: 5, flexDirection: "column", justifyContent: "flex-end", paddingRight: 0 }}>
                        <Text>ca. {artikel.dauer >= 60 ? parseInt(artikel.dauer / 60) + "h" : null} {artikel.dauer % 60 + "min"}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    })

    return (
        <ScrollView style={styles.scrollView}>
            <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: "center", paddingBottom: 10, color: "#6Fa4AB" }}>{title}</Text>
            <Text style={{ fontSize: 16, fontWeight: "bold", textAlign: "center", paddingBottom: 10, color: "#6Fa4AB" }}>(Bild anklicken, um zu öffnen)</Text>
            <Text style={textStyle.contentHeadline}>Bücher</Text>
            <ScrollView horizontal={true} persistentScrollbar={true}>
                {showBooks}
            </ScrollView>
            <Text style={textStyle.contentHeadline}>Filme</Text>
            <ScrollView horizontal={true} persistentScrollbar={true}>
                {showFilms}
            </ScrollView>
            <Text style={textStyle.contentHeadline}>Artikel</Text>
            <ScrollView horizontal={true} persistentScrollbar={true}>
                {showArtikel}
            </ScrollView>
        </ScrollView>
    )
}

const textStyle = StyleSheet.create({
    contentHeadline: {
        fontSize: 16, fontWeight: "bold",
        paddingTop: 10,
        color: "#6Fa4AB"
    }
});