import * as React from "react";
import * as Progress from "react-native-progress";
import {
    View,
    SafeAreaView,
    StyleSheet,
    Text,
    ScrollView,
    Button,
    TouchableOpacity,
} from "react-native";
import styles from "./../styles";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Questions } from "../objects/questions";

export default function ProfilScreen() {
    const [topics, setTopics] = React.useState(
        global.user.topics.filter((t) => t.id !== null)
    );
    const [reloadPage, setReloadPage] = React.useState(0); //useState, that reloads page

    var questions = new Questions();
    const [questionPercentage, setquestionPercentage] = React.useState(
        questions.topics.filter((k) => k.topicId === 1)
    );
    console.log(questionPercentage);

    const showTopicNames = topics.map((topic) => {
        return (
            <View>
                <Text style={styles.profileScreenInfos2}>{topic.name}</Text>
            </View>
        );
    });
    console.log(showTopicNames);
    const showTopicPercentage = questionPercentage.map((topics) => {
        return (
            <View style={styles.profileScreenInfos3}>
                <Progress.Bar
                    progress={
                        topics.questions.filter(
                            (question) => question.correct === true
                        ).length / 10
                    }
                    width={200}
                    showsText={true}
                    //borderWidth={2}
                    //borderRadius={0}
                />
            </View>
        );
    });

    return (
        <ScrollView
            style={styles.scrollView}
            scrollEventThrottle={100}
            onScroll={() => {
                setReloadPage(reloadPage + 1);
            }}
        >
            <FontAwesome
                style={styles.profileScreen}
                name="user-circle-o"
                size={150}
            />
            <Text style={styles.profileScreenName}>
                Profil: {global.user.userName}{" "}
            </Text>
            <Text></Text>
            <Text></Text>
            <Text></Text>
            <Text></Text>
            <Text></Text>
            <Text></Text>
            <Text style={styles.profileScreenInfos}>
                Momentan ausgewählte Themen:
                {"\n"}
            </Text>
            {showTopicNames}
            <Text></Text>
            <Text style={styles.profileScreenInfos}>
                Lernfortschritt in %: {"\n"}
            </Text>
            {showTopicPercentage}
            <Text style={styles.profileScreenInfos3}></Text>
        </ScrollView>
    );
}
