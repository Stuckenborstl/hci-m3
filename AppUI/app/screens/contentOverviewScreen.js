import * as React from "react";
import * as Progress from "react-native-progress";
import {
    ScrollView,
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Linking,
    Alert,
    Modal,
    Pressable,
    Button,
} from "react-native";
import styles from "../styles";
import { Content } from "../objects/content";
import { colors } from "../colors";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { Questions } from "../objects/questions";

//Displays Content of 1 of the personalized Topics
//Selected-Topic switch possible
//contains Quiz (using questions.ts)
export default function ContentOverviewScreen({ navigation, route }) {
    //Shows, if user tries to go to Content without Selecting a Topic first
    if (global.user.topics.find((t) => t.id !== null) === undefined)
        return (
            <View style={styles.centeredView}>
                <Text
                    style={{
                        fontSize: 24,
                        lineHeight: 36,
                        fontWeight: "bold",
                        textAlign: "center",
                        padding: 8,
                        color: colors.fehler,
                        textAlign: "center",
                    }}
                >
                    Bitte wähle zuerst ein Thema (unter Themen)
                </Text>
            </View>
        );

    const [modalVisible, setModalVisible] = React.useState(false); //Quiz visable?
    const [answered, setAnswered] = React.useState(false); //question Answered?
    const [answerCorrect, setAnsweredCorrect] = React.useState(false); //was Answer correct?
    const [reloadPage, setReloadPage] = React.useState(0); //useState, that reloads page
    const [topicIndex, setTopicIndex] = React.useState(global.user.topics.find(t => t.id !== null).id); //id of current topic
    const [loadingPercentage, setLoadingPercentage] = React.useState(0.0);

    var content = new Content;
    var questions = new Questions;
    var bookArray = content.topics.filter(t => t.id === topicIndex)[0].Buecher; //array with all books to topic - personalisationIdea: sort Array and delete some Parts
    var filmArray = content.topics.filter(t => t.id === topicIndex)[0].Filme; //array with all films to topic - personalisationIdea: sort Array and delete some Parts
    var artikelArray = content.topics.filter(t => t.id === topicIndex)[0].Artikel; //array with all articles to topic - personalisationIdea: sort Array and delete some Parts
    var questionArray = questions.topics.filter(topic => topic.topicId === topicIndex)[0].questions.filter(question => question.correct === false);
    
    const [currentQuestionId, setCurrentQuestionId] = React.useState(questionArray[0].questionId);
    const [prograssBar, setPrograssBar] = React.useState(10 - questionArray.length);

    //As far as I am concerned, image-sources can not be required dynamically, so we have to use urls
    //Displays all Books
    const showBooks = bookArray.map((book) => {
        return (
            <View
                key={book.name}
                style={{
                    margin: 10,
                    marginTop: 5,
                    marginRight: 5,
                    padding: 5,
                    backgroundColor: "#B4DFE5",
                    borderRadius: 5,
                }}
            >
                <TouchableOpacity onPress={() => Linking.openURL(book.url)}>
                    <Text>{book.name}</Text>
                    <View
                        style={{
                            padding: 5,
                            flexDirection: "row",
                            paddingRight: 0,
                        }}
                    >
                        <Image
                            source={{ uri: book.bild }}
                            style={{ width: 80, height: 80 }}
                        />
                        <View
                            style={{
                                padding: 5,
                                flexDirection: "column",
                                justifyContent: "flex-end",
                                paddingRight: 0,
                            }}
                        >
                            <Text>{book.seiten} Seiten</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    });

    //As far as I am concerned, image-sources can not be required dynamically, so we have to use urls
    //Displays all Films
    const showFilms = filmArray.map((film) => {
        return (
            <View
                key={film.name}
                style={{
                    margin: 10,
                    marginTop: 5,
                    marginRight: 5,
                    padding: 5,
                    backgroundColor: "#B4DFE5",
                    borderRadius: 5,
                }}
            >
                <TouchableOpacity onPress={() => Linking.openURL(film.url)}>
                    <Text>{film.name}</Text>
                    <View
                        style={{
                            padding: 5,
                            flexDirection: "row",
                            paddingRight: 0,
                        }}
                    >
                        <Image
                            source={{ uri: film.bild }}
                            style={{ width: 80, height: 80 }}
                        />
                        <View
                            style={{
                                padding: 5,
                                flexDirection: "column",
                                justifyContent: "flex-end",
                                paddingRight: 0,
                            }}
                        >
                            <Text>
                                {film.dauer >= 60
                                    ? parseInt(film.dauer / 60) + "h"
                                    : null}{" "}
                                {(film.dauer % 60) + "min"}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    });

    //As far as I am concerned, image-sources can not be required dynamically, so we have to use urls
    //Displays all Articles
    const showArtikel = artikelArray.map((artikel) => {
        return (
            <View
                key={artikel.name}
                style={{
                    margin: 10,
                    marginTop: 5,
                    marginRight: 5,
                    padding: 5,
                    backgroundColor: "#B4DFE5",
                    borderRadius: 5,
                }}
            >
                <TouchableOpacity onPress={() => Linking.openURL(artikel.url)}>
                    <Text>{artikel.name}</Text>
                    <View
                        style={{
                            padding: 5,
                            flexDirection: "row",
                            paddingRight: 0,
                        }}
                    >
                        <Image
                            source={{ uri: artikel.bild }}
                            style={{ width: 80, height: 80 }}
                        />
                        <View
                            style={{
                                padding: 5,
                                flexDirection: "column",
                                justifyContent: "flex-end",
                                paddingRight: 0,
                            }}
                        >
                            <Text>
                                ca.{" "}
                                {artikel.dauer >= 60
                                    ? parseInt(artikel.dauer / 60) + "h"
                                    : null}{" "}
                                {(artikel.dauer % 60) + "min"}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    });

    //ScrollView, onPress ordnet Content-Aufbereitung entsprechend des ausgewählten Topics new
    //Displays ScrollView on Top, containing all selected topics
    function showTopics() {
        return (
            <ScrollView horizontal={true} style={{ alignSelf: "center" }}>
                {global.user.topics[0].id !== null ? (
                    <TouchableOpacity
                        onPress={() => setTopicIndex(1)}
                        style={{
                            flexDirection: "row",
                            margin: 5,
                            padding: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 5,
                            backgroundColor: "#F5834E",
                        }}
                    >
                        <View
                            style={{
                                paddingHorizontal: 6,
                                backgroundColor: "white",
                                borderRadius: 10,
                            }}
                        >
                            <FontAwesome5
                                style={{ marginTop: 3 }}
                                name={"dove"}
                                size={16}
                                color={"#F5834E"}
                            />
                        </View>
                        <Text
                            style={{
                                fontWeight: "bold",
                                fontSize: 16,
                                color: "white",
                            }}
                        >
                            {" "}
                            Griechische Mythologie
                        </Text>
                    </TouchableOpacity>
                ) : null}
                {global.user.topics[1].id !== null ? (
                    <TouchableOpacity
                        onPress={() => setTopicIndex(2)}
                        style={{
                            flexDirection: "row",
                            margin: 5,
                            padding: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 5,
                            backgroundColor: "#F5834E",
                        }}
                    >
                        <View
                            style={{
                                paddingHorizontal: 6,
                                backgroundColor: "white",
                                borderRadius: 10,
                            }}
                        >
                            <FontAwesome5
                                style={{ marginTop: 3 }}
                                name={"pound-sign"}
                                size={16}
                                color={"#F5834E"}
                            />
                        </View>
                        <Text
                            style={{
                                fontWeight: "bold",
                                fontSize: 16,
                                color: "white",
                            }}
                        >
                            {" "}
                            Peaky Blinders
                        </Text>
                    </TouchableOpacity>
                ) : null}
                {global.user.topics[2].id !== null ? (
                    <TouchableOpacity
                        onPress={() => setTopicIndex(3)}
                        style={{
                            flexDirection: "row",
                            margin: 5,
                            padding: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 5,
                            backgroundColor: "#F5834E",
                        }}
                    >
                        <View
                            style={{
                                paddingHorizontal: 6,
                                backgroundColor: "white",
                                borderRadius: 10,
                            }}
                        >
                            <FontAwesome
                                style={{ marginTop: 3 }}
                                name={"anchor"}
                                size={16}
                                color={"#F5834E"}
                            />
                        </View>
                        <Text
                            style={{
                                fontWeight: "bold",
                                fontSize: 16,
                                color: "white",
                            }}
                        >
                            {" "}
                            Marco Polo
                        </Text>
                    </TouchableOpacity>
                ) : null}
                {global.user.topics[3].id !== null ? (
                    <TouchableOpacity
                        onPress={() => setTopicIndex(4)}
                        style={{
                            flexDirection: "row",
                            margin: 5,
                            padding: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 5,
                            backgroundColor: "#F5834E",
                        }}
                    >
                        <View
                            style={{
                                paddingHorizontal: 6,
                                backgroundColor: "white",
                                borderRadius: 10,
                            }}
                        >
                            <FontAwesome
                                style={{ marginTop: 3 }}
                                name={"bitcoin"}
                                size={16}
                                color={"#F5834E"}
                            />
                        </View>
                        <Text
                            style={{
                                fontWeight: "bold",
                                fontSize: 16,
                                color: "white",
                            }}
                        >
                            {" "}
                            Bitcoin
                        </Text>
                    </TouchableOpacity>
                ) : null}
                {global.user.topics[4].id !== null ? (
                    <TouchableOpacity
                        onPress={() => setTopicIndex(5)}
                        style={{
                            flexDirection: "row",
                            margin: 5,
                            padding: 5,
                            paddingLeft: 20,
                            paddingRight: 20,
                            borderRadius: 5,
                            backgroundColor: "#F5834E",
                        }}
                    >
                        <View
                            style={{
                                paddingHorizontal: 6,
                                backgroundColor: "white",
                                borderRadius: 10,
                            }}
                        >
                            <FontAwesome5
                                style={{ marginTop: 3 }}
                                name={"flask"}
                                size={16}
                                color={"#F5834E"}
                            />
                        </View>
                        <Text
                            style={{
                                fontWeight: "bold",
                                fontSize: 16,
                                color: "white",
                            }}
                        >
                            {" "}
                            Marie Curie
                        </Text>
                    </TouchableOpacity>
                ) : null}
            </ScrollView>
        );
    }

    //trigered when the Quiz has been answered
    function answerDone(correct) {
        setAnswered(true);
        setAnsweredCorrect(correct);
        questions.topics.filter(t => t.topicId === topicIndex)[0].questions.filter(q => q.questionId === currentQuestionId)[0].correct = correct;
        questionArray = questions.topics.filter(topic => topic.topicId === topicIndex)[0].questions.filter(question => question.correct === false);
        setPrograssBar(10 - questionArray.length);
        for (var i = 0; i < 9; i++) {
            setTimeout(() => { setLoadingPercentage(10 / i) }, 300)
        }
        setTimeout(() => {
            setAnsweredCorrect(false);
            setAnswered(false);
            setCurrentQuestionId(questionArray.filter(question => question.questionId > currentQuestionId)[0].questionId);
            setLoadingPercentage(0)
        }
            , 3000
        );
    };

    function showQuestion() {
        var questionObject = questionArray[currentQuestionId];
        return (
            <View>
                <View style={{ marginBottom: 10, flexDirection: "row", justifyContent: "space-around" }}>
                    <View style={{ alignSelf: "center" }}>
                        <Progress.Bar progress={prograssBar / 10} width={200} showsText={true} color={colors.fehler} />
                    </View>
                    <Text style={{ color: colors.titleBlue, fontWeight: "bold" }}>{prograssBar} / 10</Text>
                </View>
                <View style={{ marginBottom: 10 }}>
                <Text style={styles.screenUntTitle}>{questionObject.question}</Text>
                </View>
                {questionObject.answers.map(answer => (
                    <View key={answer.id}>
                        <Pressable onPress={() => answerDone(answer.correct)}>
                            <View style={{ margin: 4, padding: 5, borderRadius: 5, backgroundColor: answered ? answerCorrect ? answer.correct ? colors.quizGreen : "grey" : answer.correct ? colors.quizGreen : colors.quizRed : "#6Fa4AB" }}>
                                <Text style={{ color: "white" }}>{answer.text}</Text>
                            </View>
                        </Pressable>
                    </View>
                ))}
            </View>
        );
    }

    //displays Quiz
    //uses querstions.ts to get Questions
    function showQuiz() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setModalVisible(!modalVisible);
                    }}
                >
                    <View>
                        <View style={styles.popUp}>
                            {showQuestion()}
                            <View style={{ flexDirection: "row", marginTop: 10 }}>
                                <Pressable onPress={() => setModalVisible(!modalVisible)} style={{ marginRight: 20 }}>
                                    <View style={{ padding: 5, marginTop: 10, borderRadius: 5, backgroundColor: "#F5834E" }}>
                                        <Text style={{ fontWeight: "bold", fontSize: 16, color: "white" }}>Quiz schließen</Text>
                                    </View>
                                </Pressable>
                                <Pressable onPress={() => setCurrentQuestionId(questionArray.filter(question => question.questionId > currentQuestionId)[0].questionId)}>
                                    <View style={{ padding: 5, marginTop: 10, borderRadius: 5, backgroundColor: "#F5834E" }}>
                                        <Text style={{ fontWeight: "bold", fontSize: 16, color: "white" }}>Frage überspringen</Text>
                                    </View>
                                </Pressable>
                            </View>
                            <View style={{ marginBottom: 10, marginTop: 20 }}>
                                {answered ? <Progress.Bar progress={loadingPercentage} width={200} showsText={true} color={colors.titleBlue} /> : null}
                            </View>
                            <Text style={{ color: colors.titleBlue, fontWeight: "bold" }}>{answered ? answerCorrect ? "richtig!" : "falsch..." : null}</Text>
                        </View>
                    </View>
                </Modal>
                <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <Pressable
                        style={{ alignItems: "center", margin: 10 }}
                        onPress={() => setModalVisible(true)}
                    >
                        <View
                            style={{
                                flexDirection: "row",
                                marginTop: 10,
                                padding: 5,
                                paddingLeft: 10,
                                paddingRight: 10,
                                borderRadius: 5,
                                backgroundColor: "#F5834E",
                            }}
                        >
                            <View
                                style={{
                                    paddingHorizontal: 6,
                                    backgroundColor: "white",
                                    borderRadius: 10,
                                }}
                            >
                                <FontAwesome
                                    style={{ marginTop: 3 }}
                                    name={"question"}
                                    size={16}
                                    color={"#F5834E"}
                                />
                            </View>
                            <Text
                                style={{
                                    fontWeight: "bold",
                                    fontSize: 16,
                                    color: "white",
                                }}
                            >
                                {" "}
                                Quiz starten
                            </Text>
                        </View>
                    </Pressable>
                </View>
            </View>
        );
    }

    //return of contentOverviewScreen
    return (
        <ScrollView
            style={styles.scrollView}
            scrollEventThrottle={100}
            onScroll={() => {
                setReloadPage(reloadPage + 1);
            }}
        >
            <Text
                style={{
                    fontSize: 24,
                    fontWeight: "bold",
                    textAlign: "center",
                    paddingBottom: 10,
                    color: "#6Fa4AB",
                }}
            >
                {global.user.topics.find((t) => t.id === topicIndex).name}
            </Text>
            <Text
                style={{
                    fontSize: 18,
                    fontWeight: "bold",
                    textAlign: "center",
                    paddingBottom: 10,
                    color: "#6Fa4AB",
                }}
            >
                (Bild anklicken, um zu öffnen)
            </Text>
            {showTopics()}
            {showQuiz()}
            <Text style={textStyle.contentHeadline}>Bücher</Text>
            <ScrollView horizontal={true}>
                {showBooks}
            </ScrollView>
            <Text style={textStyle.contentHeadline}>Filme</Text>
            <ScrollView horizontal={true}>
                {showFilms}
            </ScrollView>
            <Text style={textStyle.contentHeadline}>Artikel</Text>
            <ScrollView horizontal={true}>
                {showArtikel}
            </ScrollView>
        </ScrollView>
    );
}

const textStyle = StyleSheet.create({
    //e.g. Bücher, Filme, ...
    contentHeadline: {
        fontSize: 18,
        fontWeight: "bold",
        paddingTop: 12,
        color: "#6Fa4AB",
    },
});
